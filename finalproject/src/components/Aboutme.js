import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function Aboutme (props) {

    return (
    <div align ="center" >
    <Box sx= { {height:"40%", width:"40%"}}>
        <Paper elevation ={5} >
           <h2> จัดทำโดย : {props.name} </h2>
           <h3> รหัสนักศึกษา {props.address} </h3>
           <h3> ภาควิชาวิทยาการคอมพิวเตอร์ {props.faculty} </h3>
        </Paper>
    </Box>
    </div>
    );
}

export default Aboutme;