import {Link} from "react-router-dom";

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

export function Header() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
        <Typography variant="h6">
        ยินดีต้อนรับสู่เว็บเเนะนำการ์ตูน : &nbsp;
        </Typography>
                <Link to ="/animation"> Animation</Link> 
                &nbsp;&nbsp;&nbsp;
                <Link to ="/disney">Disney</Link>
                &nbsp;&nbsp;&nbsp;
                <Link to ="/anime"> Anime </Link>
                &nbsp;&nbsp;&nbsp;
                <Link to ="/about"> ผู้จัดทำ </Link>
                &nbsp;&nbsp;&nbsp;
            <hr />
        </Toolbar>
      </AppBar>
    </Box>
  );
}
export default Header;

