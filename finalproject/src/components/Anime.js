import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';

function Anime (prop) {

    return (
    <Box sx= {{width:"50%"}}>
        <Paper elevation ={5} >
           <h1>ชื่อเรื่อง {prop.name} </h1>
           <p>เนื้อหา {prop.content} </p>
       
           </Paper>
    </Box>
    );
}

export default Anime ;