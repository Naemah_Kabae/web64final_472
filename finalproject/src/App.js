import logo from './logo.svg';
import './App.css';

import Aboutme from './components/Aboutme';
import AnimationPage from './pages/AnimationPage';
import DisneyPage from './pages/DisneyPage';
import AnimePage from './pages/AnimePage';
import Header from './components/Header';

import { Routes,Route } from 'react-router-dom';

function App() {
  return (
    <div className="App">

      <Header/>
      <Routes>
      <Route path ="animation" element ={
                  <AnimationPage />
             }/>
      <Route path ="disney" element ={
                  <DisneyPage />
             }/>
       <Route path ="anime" element ={
                  <AnimePage />
             }/>        
      <Route path="about" element ={
                  <Aboutme name = "นาอีมะห์ กาเเบ" 
                           address ="6210210472"
                           faculty = "คณะวิทยาศาสตร์" />
                  }/>
      </Routes>
    </div>
  );
}

export default App;
